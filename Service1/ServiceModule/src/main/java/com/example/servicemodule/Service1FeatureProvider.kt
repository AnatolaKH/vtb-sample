package com.example.servicemodule

import androidx.fragment.app.Fragment
import com.example.integration.CoreAppDeps
import com.example.integration.FeatureProvider
import com.example.integration.LaunchOptions

object Service1FeatureProvider: FeatureProvider {

    private var coreDeps: CoreAppDeps? = null

    override fun getFeatureScreen(launchOptions: LaunchOptions?): Fragment =
        Service1FeatureFragment()

    override fun init(coreDeps: CoreAppDeps) {
        this.coreDeps = coreDeps
    }
}