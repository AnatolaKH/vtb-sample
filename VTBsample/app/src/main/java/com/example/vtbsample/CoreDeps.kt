package com.example.vtbsample

import com.example.integration.AppConfig
import com.example.integration.CoreAppDeps

object CoreDeps : CoreAppDeps {

    override val appConfig: AppConfig = Config
}

object Config : AppConfig