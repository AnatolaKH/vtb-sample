package com.example.vtbsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.servicemodule.Service1FeatureProvider

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment = initService1ProductScreen()

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }

    private fun initService1ProductScreen(): Fragment {
        val featureProvider = Service1FeatureProvider
        featureProvider.init(CoreDeps)
        return featureProvider.getFeatureScreen()
    }
}