package com.example.integration

interface CoreAppDeps {
    val appConfig: AppConfig
}

interface AppConfig