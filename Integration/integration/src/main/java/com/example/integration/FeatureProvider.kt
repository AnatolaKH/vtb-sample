package com.example.integration

import androidx.fragment.app.Fragment

interface FeatureProvider {

    fun init(coreDeps: CoreAppDeps)

    fun getFeatureScreen(launchOptions: LaunchOptions? = null): Fragment
}